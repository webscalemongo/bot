import praw
import json
import pandas

# The ID of the post to upvote
post_url = "https://www.reddit.com/r/soccer/comments/xkbsne/immobile_i_considered_leaving_the_national_team_i/"

credentials = json.load(open("app.json"))

# read the CSV file of username and passwords, then login to each account and upvote the post
accounts = pandas.read_csv("accounts.csv")
for account in accounts.to_dict("records"):
    print(f"Logging in as {account['username']}")
    # login to the account
    reddit = praw.Reddit(
        **dict(
            **credentials,
            **dict(
                username=account["username"],
                password=account["password"],
                user_agent="testscript",
            ),
        )
    )

    # upvote the post
    submission = reddit.submission(url=post_url)
    print(f"Upvoting post {submission.title}")
    submission.upvote()
