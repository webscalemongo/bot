# Getting Started

Create a new reddit app on here: https://ssl.reddit.com/prefs/apps/

I highly recommend creating an app on an account that is not your main account. Reddit could ban the bot
and you don't want them to ban your main account.

Scroll to the bottom of that page and click "Create an App". In the form that opens put these values:

- name: whatever you want
- type: select "script" from the options
- description: whatever you want
- about url: just put something like http://localhost:8080
- callback url: just put something like http://localhost:8080, it doesn't matter

Click "create app" and you should see a page with your app's client id and client secret. Copy those.

The client secret is called `secret` on the page. The client is at the top left corner of the box underneath `personal use script`.

Open the `app.json` file in this directory and put the client id and client secret in the appropriate places.

## Install the requirements

Honestly I don't know what operating system you use. If you are on a mac, you can install python using homebrew at this link: https://brew.sh/

Once brew is installed, open a terminal window and run this command:

    brew install python

If you are on windows, you can download python from here: https://www.python.org/downloads/ but not sure if its intuitive to use. If you don't
have access to a mac or linux machine, can probably use a virtual machine to run it. 

In general, it's a good idea to use a virtual machine because if reddit black lists your IP, you can just run it from another
machine in the cloud.

## Populating the accounts

Right now it needs a list of accounts to upvote from. You could make a bunch of accounts and put their credentials
in the `accounts.csv` file. To add a new account, just add a new line to the file with the username and password separated by a comma.

## Running the bot

First install the requirements
    
    python3 -m pip install -r requirements.txt

Then open the `upvote.py` file and change the `post_url` variable to the url of the post you want to upvote. Then run the bot with:

    python3 upvote.py
